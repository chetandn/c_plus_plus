#pragma once
#include "Person.hpp"
#include <string>

class Tweeter : public Person
{
private:
    std::string tweeterhandle;
public:
    Tweeter(std::string firstname, std::string lastname, int id, std::string handle);
    ~Tweeter();
};