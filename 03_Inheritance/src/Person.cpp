#include "../inc/Person.hpp"
#include <iostream>

Person::Person(std::string firstname_, std::string lastname_, int id_)
:
firstname(firstname_),
lastname(lastname_),
id(id_)
{
    std::cout << "Constructing at base class part of Person :" << firstname << " " << lastname << std::endl;
}

Person::Person() 
: 
id(0) 
{
    std::cout << "Constructing at base clasee part of Empty Person:" << firstname << " " << lastname << std::endl;
}

Person::~Person() 
{
    std::cout << "Destructing part of base class :" << firstname << " " << lastname << std::endl;
}

std::string Person::getName()
{
    return firstname + " " + lastname;
}
