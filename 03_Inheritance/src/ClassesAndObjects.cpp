#include "../inc/Tweeter.hpp"
#include <iostream>

int main()
{
    Person p1("Chetan", "Naik", 007);
    {
        Person p2;
        Tweeter t1("Elon", "Musk", 73514, "@e_musk");
        std::cout << "name : " << t1.getName() << std::endl;
    }

    std::cout << "After scope block" << std::endl;

    std::cout << "Person 1 Name is : " << p1.getName() << std::endl;

    return 0;
}


