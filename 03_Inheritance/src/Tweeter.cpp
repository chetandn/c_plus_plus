#include "../inc/Tweeter.hpp"
#include <iostream>

Tweeter::Tweeter(std::string firstname_, std::string lastname_, int id_, std::string handle_)
:
Person(firstname_, lastname_, id_),
tweeterhandle(handle_)
{
    std::cout << "Constructing at derived class part of Tweeter :" << tweeterhandle << std::endl;
}

Tweeter::~Tweeter()
{
    std::cout << "Destructing part of Tweeter:" << tweeterhandle << std::endl;
}