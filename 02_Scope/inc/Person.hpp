#include <string>

class Person
{

private:
    std::string firstname;
    std::string lastname;
    int id;

public:
    std::string getName();
    Person(std::string firstname_, std::string lastname_, int id_);
    Person();
    ~Person();
};
