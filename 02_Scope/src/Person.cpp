#include "../inc/Person.hpp"
#include <iostream>

Person::Person(std::string firstname_, std::string lastname_, int id_)
:
firstname(firstname_),
lastname(lastname_),
id(id_)
{
    std::cout << "Constructing object called Person :" << firstname << " " << lastname << std::endl;
}

Person::Person() 
: 
id(0) 
{
    std::cout << "Constructing object called Empty Person:" << firstname << " " << lastname << std::endl;
}

Person::~Person() 
{
    std::cout << "Destructing object :" << firstname << " " << lastname << std::endl;
}

std::string Person::getName()
{
    return firstname + " " + lastname;
}
