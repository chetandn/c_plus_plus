#include "../inc/Person.hpp"
#include <iostream>

int main()
{
    Person p1("Chetan", "Naik", 007);
    {
        Person p2;
    }

    std::cout << "After scope block" << std::endl;

    std::cout << "Person 1 Name is : " << p1.getName() << std::endl;
    // std::cout << "Person 2 Name is : " << p2.getName() << std::endl;     //compilation error: ‘p2’ was not declared in this scope

    return 0;
}


// #output -
// Constructing called Person :Chetan Naik
// Constructing called Empty Person: 
// Destructing : 
// After scope block
// Person 1 Name is : Chetan Naik
// Destructing :Chetan Naik