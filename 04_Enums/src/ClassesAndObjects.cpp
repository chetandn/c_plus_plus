#include "../inc/status.hpp"
#include <iostream>

int main()
{
    std::cout << "In Enum main " << std::endl;

    status s = pending;
    s = approved;

    FileError fe1 = FileError::notfound;
    fe1 = FileError::ok;

    NetworkError ne1 = NetworkError::disconnected;
    ne1 = NetworkError::ok;


    return 0;
}

