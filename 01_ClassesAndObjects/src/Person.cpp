#include "../inc/Person.hpp"

Person::Person(std::string firstname_, std::string lastname_, int id_)
:
firstname(firstname_),
lastname(lastname_),
id(id_)
{
}

std::string Person::getName()
{
    return firstname + " " + lastname;
}
