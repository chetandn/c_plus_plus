#include "../inc/Person.hpp"
#include <iostream>

int main()
{
    Person p1("Chetan", "Naik", 007);
    Person p2;

    std::cout << "Person 1 Name is : " << p1.getName() << std::endl;
    std::cout << "Person 2 Name is : " << p2.getName() << std::endl;
}
