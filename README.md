Environment Used -
Ubuntu 16.04

Steps to follow while building the application-

Step 1 - Create a build folder in each project and navigate to it.
	 Eg - $mkdir <ProjectFolder>/build/
	      $cd <ProjectFolder>/build

Step 2 - Then run the CMake
	 Eg - $cmake ..

Step 3 - The run the make
	 Eg - $make

Step 4 - Run the application generated
	 Eg - $./mainApp
